from django.apps import AppConfig


class TransaksirsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksirs'
