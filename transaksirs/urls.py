from django.urls import path

from . import views

app_name = 'transaksirs'

urlpatterns = [
    path('', views.read, name='read'),
    path('update/<str:idtransaksi>/<str:tglmasuk>', views.update, name='update'),
    path('updatePost/', views.updatePost, name='updatePost'),
    path('delete/<str:idtransaksi>/<str:tglmasuk>', views.delete, name='delete'),
]