from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt
import datetime

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def formatDateSQL(tgl):
    temp = list()
    for x in tgl.split("-"):
        temp.append(int(x))
    return temp

def read(request):
    query = "SELECT * from transaksi_rs"
    with connection.cursor() as cursor:
        cursor.execute(query)
        data = dictfetchall(cursor)
    return render(request, 'transaksirs/read.html', {'data':data})

def update(request, idtransaksi, tglmasuk):
    #tampilan form update)
    print(tglmasuk)
    tglmasuk = formatDateSQL(tglmasuk)
    dateTglMasuk = datetime.date(tglmasuk[0], tglmasuk[1], tglmasuk[2])
    with connection.cursor() as cursor:
        cursor.execute("Select * from transaksi_rs where idtransaksi = %s AND tglmasuk = %s", 
        [idtransaksi, dateTglMasuk])
        res = dictfetchall(cursor)
    
    dataGet = {
        'idtransaksi' : res[0]['idtransaksi'],
        'kodepasien' : res[0]['kodepasien'],
        'tanggalpembayaran' : res[0]['tanggalpembayaran'],
        'waktupembayaran' : res[0]['waktupembayaran'],
        'tglmasuk' : res[0]['tglmasuk'],
        'totalbiaya' : res[0]['totalbiaya'],
        'statusbayar' : res[0]['statusbayar'],
    }
    print(dataGet)
    return render(request, 'transaksirs/update.html', {'data': dataGet})

@csrf_exempt
def updatePost(request):
    if request.method == "POST":
        data = request.POST
        print(data)
        idtransaksi = str(data['idtransaksi'])
        tglmasuk = formatDateSQL(str(data['tglmasuk']))
        statusbayar = str(data['statusbayar'])

        dateTglMasuk = datetime.date(tglmasuk[2], tglmasuk[1], tglmasuk[0])
    
        with connection.cursor() as cursor:
            cursor.execute("UPDATE transaksi_rs set statusbayar= %s where idtransaksi = %s AND tglmasuk = %s",
            [statusbayar, idtransaksi, dateTglMasuk])
        return redirect("transaksirs:read")

def delete(request, idtransaksi, tglmasuk):
    query = "DELETE FROM transaksi_rs WHERE idtransaksi = '" + idtransaksi + "' AND tglmasuk = '" + tglmasuk + "'" 
    with connection.cursor() as cursor:
        cursor.execute(query)
        return redirect("transaksirs:read")