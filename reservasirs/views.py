from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
import datetime
from django.http import JsonResponse
import json


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def formatDateSQL(tgl):
    temp = list()
    for x in tgl.split("-"):
        temp.append(int(x))
    return temp

class TglKeluarSalah(Exception):
    """Raised when the input tglkeluar is smaller than tglmasuk"""

    def __init__(self, message="Tanggal Keluar Harus Lebih Besar Dibanding Tanggak Masuk"):
            self.message = message
            super().__init__(self.message)

def read(request):
    query = "SELECT * from reservasi_rs"
    with connection.cursor() as cursor:
        cursor.execute(query)
        data = dictfetchall(cursor)
    return render(request, 'reservasirs/read.html', {'data':data})

@csrf_exempt
def create(request):
    if request.method == "POST":
        data = request.POST
        kodepasien = str(data['kodepasien'])
        tglmasuk = formatDateSQL(str(data['tglmasuk']))
        tglkeluar = formatDateSQL(str(data['tglkeluar']))
        koders = str(data['koders'])
        koderuangan = str(data['koderuangan'])
        kodebed = str(data['kodebed'])

        dateTglMasuk = datetime.date(tglmasuk[0], tglmasuk[1], tglmasuk[2])
        dateTglKeluar = datetime.date(tglkeluar[0], tglkeluar[1], tglkeluar[2])

        try:
            if (dateTglKeluar - dateTglMasuk).days < 0:
                raise TglKeluarSalah
        except TglKeluarSalah:
            print("hallo")
            messages.add_message(request, 
            messages.WARNING, "Error: Tanggal Keluar Harus Lebih Besar Dibanding Tanggak Masuk")
            return redirect('reservasirs:create')
        
        with connection.cursor() as cursor:
            # cursor.execute(query)
            cursor.execute("INSERT INTO reservasi_rs VALUES (%s, %s, %s, %s, %s, %s);", 
            [kodepasien, dateTglMasuk, dateTglKeluar, koders, koderuangan, kodebed])

        return redirect("reservasirs:read")

    #tampilan form create
    queryNik = "SELECT nik FROM PASIEN"
    queryKoders = "SELECT kode_faskes FROM RUMAH_SAKIT"
    with connection.cursor() as cursor:
        cursor.execute(queryNik)
        nik = dictfetchall(cursor)

        cursor.execute(queryKoders)
        koders = dictfetchall(cursor)

    return render(request, "reservasirs/createAjax.html", 
    {'nik' : nik,
    'koders' : koders
    })
    

def update(request, kodepasien, tglmasuk):
    #tampilan form update
    tglmasuk = formatDateSQL(tglmasuk)
    dateTglMasuk = datetime.date(tglmasuk[0], tglmasuk[1], tglmasuk[2])
    with connection.cursor() as cursor:
        cursor.execute("Select * from reservasi_rs where kodepasien = %s AND tglmasuk = %s",
        [kodepasien, dateTglMasuk])
        res = dictfetchall(cursor)
    dataGet = {
        'kodepasien' : res[0]['kodepasien'],
        'tglmasuk' : res[0]['tglmasuk'],
        'tglkeluar' : res[0]['tglkeluar'],
        'koders' : res[0]['koders'],
        'koderuangan' : res[0]['koderuangan'],
        'kodebed' : res[0]['kodebed']
    }
    return render(request, 'reservasirs/update.html', {'data': dataGet})

@csrf_exempt
def updatePost(request):
    if request.method == "POST":
        data = request.POST
        kodepasien = str(data['kodepasien'])
        tglmasuk = formatDateSQL(str(data['tglmasuk']))
        tglkeluar = formatDateSQL(str(data['tglkeluar']))

        dateTglMasuk = datetime.date(tglmasuk[0], tglmasuk[1], tglmasuk[2])
        dateTglKeluar = datetime.date(tglkeluar[0], tglkeluar[1], tglkeluar[2])

        try:
            if (dateTglKeluar - dateTglMasuk).days < 0:
                raise TglKeluarSalah
        except TglKeluarSalah:
            messages.add_message(request, 
            messages.WARNING, "Error: Tanggal Keluar Harus Lebih Besar Dibanding Tanggak Masuk")
            return redirect('reservasirs:update', kodepasien, data['tglmasuk'])

        jumlah_hari = int((dateTglKeluar - dateTglMasuk).days)
        totalbiaya = str(jumlah_hari * 500000)
        
        with connection.cursor() as cursor:
            cursor.execute("UPDATE reservasi_rs set tglkeluar= %s where kodepasien = %s AND tglmasuk = %s",
            [dateTglKeluar, kodepasien, dateTglMasuk])
        #update totalbiaya akibat update tgl
            cursor.execute("UPDATE transaksi_rs set totalbiaya= %s where kodepasien = %s AND tglmasuk = %s",
            [totalbiaya, kodepasien, dateTglMasuk])

        return redirect("reservasirs:read")

def delete(request, kodepasien, tglmasuk):
    queryChild = "DELETE FROM transaksi_rs WHERE kodepasien = '" + kodepasien + "' AND tglmasuk = '" + tglmasuk + "'"
    query = "DELETE FROM reservasi_rs WHERE kodepasien = '" + kodepasien + "' AND tglmasuk = '" + tglmasuk + "'" 
    with connection.cursor() as cursor:
        cursor.execute(queryChild)
        cursor.execute(query)
        return redirect("reservasirs:read")

def getkoderuangan(request):
    koders = request.GET['q']
    koderuangan = ""

    with connection.cursor() as cursor:
        cursor.execute("""
            SELECT DISTINCT koderuangan FROM RUANGAN_RS
            WHERE koders = %s ORDER BY koderuangan ASC""",
            [koders])

        data = cursor.fetchall()
        print(data)
    
    context = {'koderuangan' : data}
    print(type(json.dumps(context)))
    print(json.dumps(context))

    return JsonResponse(json.dumps(context), safe=False)


def getkodebed(request):
    koders = request.GET['q']
    kodebed = ""

    with connection.cursor() as cursor:
        cursor.execute("""
            SELECT DISTINCT kodebed FROM BED_RS
            WHERE koders = %s ORDER BY kodebed ASC""",
            [koders])

        data = cursor.fetchall()
        print(data)
    
    context = {'kodebed' : data}
    print(type(json.dumps(context)))
    print(json.dumps(context))

    return JsonResponse(json.dumps(context), safe=False)
