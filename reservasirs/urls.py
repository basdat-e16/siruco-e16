from django.urls import path

from . import views

app_name = 'reservasirs'

urlpatterns = [
    path('', views.read, name='read'),
    path('create/', views.create, name='create'),
    path('update/<str:kodepasien>/<str:tglmasuk>', views.update, name='update'),
    path('updatePost/', views.updatePost, name='updatePost'),
    path('delete/<str:kodepasien>/<str:tglmasuk>', views.delete, name='delete'),
    path('getkoderuangan/', views.getkoderuangan, name='getkoderuangan'),
    path('getkodebed/', views.getkodebed, name='getkodebed'),
]