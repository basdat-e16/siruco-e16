$(document).on('click', ".detail-url", function () {
    var td = $(this).parent();
    var tr = $(td).parent();
    
    var idTEle = $(tr).find(".idT");
    var idT = $(idTEle).text();
    
    var idTMEle = $(tr).find(".idTM");
    var idTM = $(idTMEle).text();

    var idNoEle = $(tr).find(".idNo");
    var idNo = $(idNoEle).text();

    window.location = `/transaksi-makan/detail/${idT}/${idTM}/`

});

$(document).on('click', ".update-url", function () {
    var td = $(this).parent();
    var tr = $(td).parent();
    
    var idTEle = $(tr).find(".idT");
    var idT = $(idTEle).text();
    
    var idTMEle = $(tr).find(".idTM");
    var idTM = $(idTMEle).text();

    var idNoEle = $(tr).find(".idNo");
    var idNo = $(idNoEle).text();

    window.location = `/transaksi-makan/update/${idT}/${idTM}/`

});

$(document).on('click', ".delete-url", function () {
    var td = $(this).parent();
    var tr = $(td).parent();
    
    var idTEle = $(tr).find(".idT");
    var idT = $(idTEle).text();
    
    var idTMEle = $(tr).find(".idTM");
    var idTM = $(idTMEle).text();

    $.ajax({
        type: 'DELETE',
        url: `/transaksi-makan/delete/${idT}/${idTM}/`,
        success: function (data) {
            window.location.reload();
        },
        Error: function(data){
            console.log("error");
        },
        fail: function(data){
            console.log("fail");
        }
    });


});