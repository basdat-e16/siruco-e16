$(document).on('click', ".update-url", function () {
    var td = $(this).parent();
    var tr = $(td).parent();
    
    var kodeHotelEle = $(tr).find(".kode-hotel");
    var kodeHotel = $(kodeHotelEle).text().trim();
    console.log(kodeHotel.trim())
    var kodePaketEle = $(tr).find(".kode-paket");
    var kodePaket = $(kodePaketEle).text(); 
    console.log(kodePaket)

    window.location = `/paket-makan/update/${kodeHotel}/${kodePaket}/`

});

$(document).on('click', ".delete-url", function () {
    var td = $(this).parent();
    var tr = $(td).parent();
    
    var kodeHotelEle = $(tr).find(".kode-hotel");
    var kodeHotel = $(kodeHotelEle).text().trim();
    console.log(kodeHotel.trim())
    var kodePaketEle = $(tr).find(".kode-paket");
    var kodePaket = $(kodePaketEle).text(); 
    console.log(kodePaket)

    $.ajax({
        type: 'DELETE',
        url: `/paket-makan/delete/${kodeHotel}/${kodePaket}/`,
        success: function (data) {
            window.location.reload();
        },
        Error: function(data){
            console.log("error");
        },
        fail: function(data){
            console.log("fail");
        }
    });

});