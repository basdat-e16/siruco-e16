from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.db.utils import IntegrityError, InternalError
import traceback

# Create your views here.

def index(request):
    return render(request, "pasien/index.html")

def readPasien(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
    
    resultList = []

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT nik, nama FROM PASIEN WHERE idpendaftar = '{request.session.get("username")}'
            """)

            result = cursor.fetchall()

            for pasien in result:
                resultList.append([pasien[0], pasien[1]])
            
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "pasien/listpasien.html", {"listPasien" : resultList})   
            

def createPasien(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT * FROM PASIEN WHERE nik = '{request.POST["nik"]}'
                """)
                result = len(cursor.fetchall())
                print(request.POST["nik"])
                print(result)
                if result == 0:
                    cursor.execute(f"""
                        INSERT INTO PASIEN
                        (idpendaftar, nik, nama, notelp, nohp, 
                        ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, ktp_prov,
                        dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov) VALUES
                        ('{request.session.get('username')}', '{request.POST['nik']}', '{request.POST['nama']}', '{request.POST['notelp']}', '{request.POST['nohp']}',
                        '{request.POST['ktp_jalan']}', '{request.POST['ktp_kelurahan']}', '{request.POST['ktp_kecamatan']}', '{request.POST['ktp_kabkot']}', '{request.POST['ktp_prov']}',
                        '{request.POST['dom_jalan']}', '{request.POST['dom_kelurahan']}', '{request.POST['dom_kecamatan']}', '{request.POST['dom_kabkot']}', '{request.POST['dom_prov']}')
                    """)

                    messages.add_message(request, messages.SUCCESS, f"Pendaftaran Pasien Berhasil")
            except InternalError:
                messages.add_message(request, messages.WARNING, "Terdapat internal error")
                traceback.print_exc()
            except :
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()
    return render(request, "pasien/createpasien.html")

def deletePasien(request, pk):
    print(pk)

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                DELETE FROM PASIEN WHERE nik = '{pk}'
            """)
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return redirect("pasien:readPasien")

def detailPasien(request, pk):

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT idpendaftar, nik, nama, notelp, nohp,
                ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, ktp_prov
                dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov
                FROM PASIEN WHERE nik = '{pk}'
            """)

            result = cursor.fetchall()
            # print(type(result))
            # print(result)
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
            print("exec happened")
    print(result)
    return render(request, "pasien/detailpasien.html", {'dataPasien' : result[0]})

def updatePasien(request, pk):

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT idpendaftar, nik, nama, notelp, nohp,
                ktp_jalan, ktp_kelurahan, ktp_kecamatan, ktp_kabkot, ktp_prov
                dom_jalan, dom_kelurahan, dom_kecamatan, dom_kabkot, dom_prov
                FROM PASIEN WHERE nik = '{pk}'
            """)

            result = cursor.fetchall()
            # print(type(result))
            # print(result)
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
            print("exec happened")

    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                """
                '{request.POST['nik']}', 
                '{request.POST['nama']}',
                '{request.POST['notelp']}', 
                '{request.POST['nohp']}',
                '{request.POST['ktp_jalan']}',
                '{request.POST['ktp_kelurahan']}',
                '{request.POST['ktp_kecamatan']}', 
                '{request.POST['ktp_kabkot']}', 
                '{request.POST['ktp_prov']}',
                '{request.POST['dom_jalan']}', 
                '{request.POST['dom_kelurahan']}', 
                '{request.POST['dom_kecamatan']}', 
                '{request.POST['dom_kabkot']}', 
                '{request.POST['dom_prov']}')
                """
                cursor.execute(f"""
                    UPDATE PASIEN
                    SET notelp = '{request.POST['notelp']}',
                    nohp = '{request.POST['nohp']}',
                    ktp_jalan = '{request.POST['ktp_jalan']}',
                    ktp_kelurahan = '{request.POST['ktp_kelurahan']}',
                    ktp_kecamatan = '{request.POST['ktp_kecamatan']}', 
                    ktp_kabkot =  '{request.POST['ktp_kabkot']}', 
                    ktp_prov = '{request.POST['ktp_prov']}',
                    dom_jalan = '{request.POST['dom_jalan']}', 
                    dom_kelurahan = '{request.POST['dom_kelurahan']}',
                    dom_kecamatan = '{request.POST['dom_kecamatan']}', 
                    dom_kabkot =  '{request.POST['dom_kabkot']}', 
                    dom_prov = '{request.POST['dom_prov']}'
                    WHERE nik = '{pk}'
                """)
            except:
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()
                print("exec happened")
                return render(request, "pasien/updatepasien.html", {'dataPasien' : result[0]})
        return redirect("pasien:readPasien")

    return render(request, "pasien/updatepasien.html", {'dataPasien' : result[0]})
    