from django.urls import path

from . import views

app_name = 'pasien'

urlpatterns = [
    path('create/', views.createPasien, name='createPasien'),
    path('read/', views.readPasien, name='readPasien'),
    path('detail/<pk>/', views.detailPasien, name='detailPasien'),
    path('update/<pk>/', views.updatePasien, name='updatePasien'),
    path('delete/<pk>/', views.deletePasien, name='deletePasien')
]