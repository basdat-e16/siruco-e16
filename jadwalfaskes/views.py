from django.shortcuts import render, redirect
from django.db import connection
import datetime
from django.views.decorators.csrf import csrf_exempt

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def formatDateSQL(tgl):
    temp = list()
    for x in tgl.split("-"):
        temp.append(int(x))
    return temp

def read(request):
    query = "SELECT * from JADWAL"
    with connection.cursor() as cursor:
        cursor.execute(query)
        data = dictfetchall(cursor)
    return render(request, 'jadwalfaskes/read.html', {'data':data})

@csrf_exempt
def create(request):
    if request.method == "POST":
        data = request.POST
        print(data)
        kode_faskes = str(data['kode_faskes'])
        shift = str(data['shift'])
        tgl = formatDateSQL(str(data['tanggal']))

        dateTgl = datetime.date(tgl[0], tgl[1], tgl[2])

        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO JADWAL VALUES (%s, %s, %s);", 
            [kode_faskes, shift, dateTgl])

        return redirect("jadwalfaskes:read")

    #tampilan form create
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode FROM FASKES")
        data = dictfetchall(cursor)

    print(data)
    return render(request, "jadwalfaskes/create.html", 
    {'faskes' : data,
    'message' : "FORM BUAT JADWAL FASKES"
    })