from django.apps import AppConfig


class JadwalfaskesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'jadwalfaskes'
