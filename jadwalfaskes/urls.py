from django.urls import path

from . import views

app_name = 'jadwalfaskes'

urlpatterns = [
    path('', views.read, name='read'),
    path('create/', views.create, name='create'),
]