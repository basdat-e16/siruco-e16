from django.urls import path

from . import views

app_name = 'rs'

urlpatterns = [
    path('', views.read, name='read'),
    path('create/', views.create, name='create'),
    path('update/<str:kode_faskes>', views.update, name='update'),
    path('updatePost/', views.updatePost, name='updatePost'),
]