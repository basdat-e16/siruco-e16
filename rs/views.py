from django.shortcuts import render, redirect
from django.db import connection
from collections import namedtuple
from django.views.decorators.csrf import csrf_exempt
import datetime

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def read(request):
    query = """SELECT *,
            CASE WHEN isrujukan = '1' THEN true ELSE false
            END checklist from rumah_sakit"""
    with connection.cursor() as cursor:
        cursor.execute(query)
        data = dictfetchall(cursor)
    return render(request, 'rs/read.html', {'data':data})

@csrf_exempt
def create(request):
    if request.method == "POST":
        data = request.POST
        print(data)
        kode_faskes = str(data['kode_faskes'])
        try:
            isrujukan = str(data['isrujukanTrue'])
        except:
            isrujukan = "0"
        
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO rumah_sakit VALUES (%s, %s);", 
            [kode_faskes, isrujukan])

        return redirect("rs:read")

    #tampilan form create
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode FROM FASKES")
        kode_faskes = dictfetchall(cursor)

    return render(request, "rs/create.html", 
    {'kode_faskes' : kode_faskes,
    'message' : "FORM BUAT RUMAH SAKIT"
    })
    

def update(request, kode_faskes):
    #tampilan form update
    query = """SELECT *,
            CASE WHEN isrujukan = '1' THEN true ELSE false
            END checklist from rumah_sakit WHERE kode_faskes = %s"""
    with connection.cursor() as cursor:
        cursor.execute(query, [kode_faskes])
        res = dictfetchall(cursor)
    dataGet = {
        'kode_faskes' : res[0]['kode_faskes'],
        'isrujukan' : res[0]['isrujukan'],
        'checklist' : res[0]['checklist'],
    }
    print(dataGet)
    return render(request, 'rs/update.html', {'data': dataGet})

@csrf_exempt
def updatePost(request):
    if request.method == "POST":
        data = request.POST
        print(data)
        kode_faskes = str(data['kode_faskes'])
        try:
            isrujukan = str(data['isrujukanTrue'])
        except:
            isrujukan = "0"
        
        with connection.cursor() as cursor:
            cursor.execute("UPDATE RUMAH_SAKIT set isrujukan= %s where kode_faskes = %s",
            [isrujukan, kode_faskes])
        return redirect("rs:read")
