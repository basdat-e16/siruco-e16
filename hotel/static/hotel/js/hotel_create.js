$(document).on('click', "#submit", function () {
    var json = {}
    var kodeHotel = $('#kode-hotel').text();
    var namaHotel = $('#nama-hotel').val();
    var isRujukan;
    if ($('#isrujukan').is(":checked")){
        isRujukan = 1;
    } else {
        isRujukan = 0;
    } 
    var jalan = $('#jalan').val();
    var kelurahan = $('#kelurahan').val();
    var kecamatan = $('#kecamatan').val();
    var kabkot = $('#kabkot').val();
    var provinsi = $('#provinsi').val();
    if (namaHotel.length == 0 ){
        alert("Nama hotel tidak boleh kosong!");
        return false;
    }
    if (jalan.length == 0 ){
        alert("Jalan tidak boleh kosong!");
        return false;
    }
    if (kelurahan.length == 0 ){
        alert("Kelurahan tidak boleh kosong!");
        return false;
    }
    if (kecamatan.length == 0 ){
        alert("Kecamatan tidak boleh kosong!");
        return false;
    }
    if (kabkot.length == 0 ){
        alert("Kabupaten/Kota tidak boleh kosong!");
        return false;
    }
    if (provinsi.length == 0 ){
        alert("Provinsi tidak boleh kosong!");
        return false;
    }
    json['kodeHotel'] = kodeHotel
    json['namaHotel'] = namaHotel
    json['isRujukan'] = isRujukan
    json['jalan'] = jalan
    json['kelurahan'] = kelurahan
    json['kecamatan'] = kecamatan
    json['kabkot'] = kabkot
    json['provinsi'] = provinsi

    json = JSON.stringify(json);
    console.log(json);

    $.ajax({
        type: 'POST',
        url: "/hotel/create/submit/",
        data: json,
        success: function (data) {
            window.location = `/hotel/`
        },
        Error: function(data){
            console.log("error");
        },
        fail: function(data){
            console.log("fail");
        }
    });
})
