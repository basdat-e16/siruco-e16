$(document).on('click', ".update-url", function () {
    var td = $(this).parent();
    var tr = $(td).parent();
    
    var kodeHotelEle = $(tr).find(".kode-hotel");
    var kodeHotel = $(kodeHotelEle).text();

    window.location = `/hotel/update/${kodeHotel}/`

});
