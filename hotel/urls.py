from django.urls import path

from . import views

app_name = 'hotel'

urlpatterns = [
    path('', views.index, name='index'),
    path("create/", views.create_hotel,name="create_hotel"),
    path("create/submit/", views.submit_create_hotel, name="submit_create_hotel"),
    path("update/<str:kode_hotel>/", views.update_hotel,name="update_hotel"),
    path("update/post/submit/", views.submit_update_hotel,name="submit_update_hotel"),
    path('kodeRuangan/', views.kodeRuangan, name='kodeRuanganAsync'),
    path('createHotelRoom/', views.createHotelRoom, name='createHotelRoom'),
    path('readHotelRoom/', views.readHotelRoom, name='readHotelRoom'),
    path('updateHotelRoom/<param1>/<param2>', views.updateHotelRoom, name='updateHotelRoom'),
    path('deleteHotelRoom/<str:param1>/<str:param2>', views.deleteHotelRoom, name='deleteHotelRoom'),
    path('createHotelReservation/', views.createHotelReservation, name='createHotelReservation'),
    path('kodeRuanganAll/', views.getAllKodeRuangan, name='kodeRuanganAll'),
    path('readHotelReservation/', views.readHotelReservation, name='readHotelReservation'),
    path('updateHotelReservation/<param1>/<param2>', views.updateHotelReservation, name='updateHotelReservation'),
    path('deleteHotelReservation/<param1>/<param2>', views.deleteHotelReservation, name='deleteHotelReservation'),
    path('readTransaksiHotel/', views.readTransaksiHotel, name='readTransaksiHotel'),
    path('updateTransaksiHotel/<pk>', views.updateTransaksiHotel, name='updateTransaksiHotel'),
    path('readTransaksiBooking', views.readTransaksiBooking, name='readTransaksiBooking')
]