from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.http import JsonResponse
import json
import traceback
from datetime import datetime
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def index(request):
    admin_sistem = False
    public_dan_satgas = False


    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")
    
    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from pengguna_publik
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            public_dan_satgas = True

        
    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            public_dan_satgas = True

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            admin_sistem = True

    if (not (admin_sistem or public_dan_satgas)):
        return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select kode, nama, isrujukan, 
            concat(jalan, ', ', kelurahan, ', ', kecamatan, ', ', kabkot, ', ', prov)
            from hotel
            order by kode asc;
        """)

        row = cursor.fetchall()
        
        hotel_query = [
            (no, item[0], item[1], item[2], item[3]) for (no, item)
            in enumerate(row, start=1)
        ]
    
            
        print(hotel_query)
        context = {
            "hotel_query" : hotel_query,  
            "admin_sistem" : admin_sistem,
            "public_dan_satgas" : public_dan_satgas
        }

    return render(request, "hotel/index.html", context=context)   

def create_hotel(request):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("hotel:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select kode
            from hotel
            order by kode desc
            limit 1;
        """)
        row = cursor.fetchall()
        max_id = row[0][0]
        max_id = "HTL-6"
        max_id = max_id.split("-")[1]
        max_id = 1 + int(max_id)
        if max_id < 10:
            max_id = str('HTL-' + str(max_id))
        else:
            max_id = str('HT-' + str(max_id))
    context = {
        "max_id" : max_id,
    }
    return render(request, "hotel/create.html", context=context)


@csrf_exempt
def submit_create_hotel(request):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("hotel:index")

    if request.is_ajax and request.method == "POST":
        data = json.loads(request.body.decode('utf-8'))
        print(data)
        kode_hotel = data.get("kodeHotel")
        nama_hotel = data.get("namaHotel")
        is_rujukan = data.get("isRujukan")
        jalan = data.get("jalan")
        kelurahan = data.get("kelurahan")
        kecamatan = data.get("kecamatan")
        kabkot = data.get("kabkot")
        provinsi = data.get("provinsi")
        
        with connection.cursor() as cursor:
            cursor.execute(f"""
                INSERT INTO HOTEL(Kode, Nama, isRujukan, Jalan, Kelurahan, Kecamatan, KabKot, Prov) VALUES
                ('{kode_hotel}', '{nama_hotel}', '{is_rujukan}', '{jalan}', '{kelurahan}', '{kecamatan}', '{kabkot}', '{provinsi}');
            """)
        return JsonResponse({}, status = 200)

@csrf_exempt
def update_hotel(request, kode_hotel):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("hotel:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from hotel
            where kode = '{kode_hotel}';
        """)
        row = cursor.fetchall()
        print(row)
        hotel = row[0]
    context = {
        "hotel" : hotel,
    }
    return render(request, "hotel/update.html", context=context)

@csrf_exempt
def submit_update_hotel(request):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("hotel:index")

    if request.is_ajax and request.method == "POST":
        data = json.loads(request.body.decode('utf-8'))
        print(data)
        kode_hotel = data.get("kodeHotel")
        nama_hotel = data.get("namaHotel")
        is_rujukan = data.get("isRujukan")
        jalan = data.get("jalan")
        kelurahan = data.get("kelurahan")
        kecamatan = data.get("kecamatan")
        kabkot = data.get("kabkot")
        provinsi = data.get("provinsi")

        
        with connection.cursor() as cursor:
            cursor.execute(f"""
                UPDATE hotel
                SET nama = '{nama_hotel}',
                isrujukan = '{is_rujukan}',
                jalan = '{jalan}',
                kelurahan = '{kelurahan}',
                kecamatan = '{kecamatan}',
                kabkot = '{kabkot}',
                prov = '{provinsi}'
                WHERE kode = '{kode_hotel}';
            """)
        return JsonResponse({}, status = 200)


def createHotelRoom(request):

    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                print(f"{request.POST['kodeHotel']}, {request.POST['jenisBed']}, {request.POST['tipe']}, {request.POST['harga']}")
                cursor.execute(f"""
                    INSERT INTO HOTEL_ROOM (kodehotel, koderoom, jenisbed, tipe, harga) VALUES(
                        '{request.POST['kodeHotel']}', '{request.POST['kodeRuangan']}', '{request.POST['jenisBed']}', '{request.POST['tipe']}', '{request.POST['harga']}'
                    )
                """)
            except:
                traceback.print_exc()
                messages.add_message(request, messages.WARNING, "Terdapat internal error")
        
        # return redirect('hotel:readHotelRoom')

    kodeList = []

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kode FROM HOTEL
            """)

            for kode in cursor.fetchall():
                kodeList.append(kode[0])
            
        except:
            pass
    
    return render(request, "hotel/createHotelRoom.html", {'kodeList' : kodeList})

def kodeRuangan(request):
    kodeHotel = request.GET['q']
    kodeRoom = ""

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT DISTINCT koderoom FROM HOTEL_ROOM
                WHERE kodehotel = '{kodeHotel}'
                ORDER BY koderoom DESC LIMIT 1;
            """)

            row = cursor.fetchone()

            print(row)

            if row != None:
                kodeRoom = row[0][0:2] + str(int(row[0][2:])+1).zfill(3)
            else :
                kodeRoom = "RH001"
            
            print("koderoom adalah" + kodeRoom)

        except:
            print("exec happened on data")
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    
    context = {'kodeRoom' : kodeRoom}

    return JsonResponse(json.dumps(context), safe=False)

def readHotelRoom(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
    
    resultList = []

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kodehotel, koderoom, jenisbed, tipe, harga
                FROM HOTEL_ROOM
            """)

            result = cursor.fetchall()

            cursor.execute(f"""
                SELECT DISTINCT koderoom from RESERVASI_HOTEL
                WHERE tglkeluar > now()::date
            """)

            undeletable = []
            deleteResult = cursor.fetchall()
            for row in deleteResult:
                undeletable.append(row[0])

            for hotelRoom in result:
                resultList.append(
                    [hotelRoom[0],
                     hotelRoom[1], 
                     hotelRoom[2], 
                     hotelRoom[3], 
                     hotelRoom[4],
                     False if hotelRoom[1] in undeletable else True
                     ])

            print(result)
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    
    return render(request, "hotel/readHotelRoom.html", {'listHotelRoom' : resultList})

def updateHotelRoom(request, param1, param2):

    context = []

    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    UPDATE HOTEL_ROOM
                    SET jenisbed = '{request.POST['jenisBed']}',
                    tipe = '{request.POST['tipe']}',
                    harga = '{request.POST['harga']}'
                    WHERE (kodehotel, koderoom) = ('{request.POST['kodehotel']}', '{request.POST['koderoom']}')
                """)

                return redirect("hotel:readHotelRoom")
            except:
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kodehotel, koderoom, jenisbed, tipe, harga
                FROM HOTEL_ROOM
                WHERE (kodehotel, koderoom) = ('{param1}', '{param2}')
            """)

            result = cursor.fetchall()[0]
            context = {'hotelRoomDetail' : result}

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, 'hotel/updateHotelRoom.html', context)

def deleteHotelRoom(request, param1, param2):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                DELETE FROM HOTEL_ROOM
                WHERE (kodehotel, koderoom) = ('{param1}', '{param2}')
            """)

            return redirect("hotel:readHotelRoom")
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
        
    return redirect("hotel:readHotelRoom")

def createHotelReservation(request):
    
    peran = request.session.get("peran")

    if peran not in ("pengguna publik", "admin satgas"):
        return redirect("pengguna:index")
    
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    INSERT INTO RESERVASI_HOTEL (kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom) VALUES
                    ('{request.POST['nikPasien']}', 
                    '{request.POST['tglmasuk']}', 
                    '{request.POST['tglkeluar']}', 
                    '{request.POST['kodehotel']}', 
                    '{request.POST['koderuangan']}')
                """)

                return redirect("hotel:readHotelReservation")
            except:
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()

    resultPasien = []
    resultHotel = []
    
    with connection.cursor() as cursor:
        try:
            
            if peran == "pengguna publik":
                cursor.execute(f"""
                    SELECT nik FROM PASIEN
                    WHERE idpendaftar = '{request.session.get("username")}'
                """)
            elif peran == "admin satgas":
                cursor.execute(f"""
                    SELECT nik FROM PASIEN
                """)
            
            resultPasien = cursor.fetchall()

            cursor.execute(f"""
                SELECT kode from HOTEL
            """)

            resultHotel = cursor.fetchall()

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "hotel/createHotelReservation.html", {'pasienList':resultPasien, 'hotelList':resultHotel})

def getAllKodeRuangan(request):
    kodeHotel = request.GET['q']
    kodeRoom = ""

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT DISTINCT koderoom FROM HOTEL_ROOM
                WHERE kodehotel = '{kodeHotel}'
                ORDER BY koderoom ASC
            """)

            row = cursor.fetchall()

            #print(row)
        
        except:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    
    context = {'kodeRoom' : row}
    print(type(json.dumps(context)))
    print(json.dumps(context))

    return JsonResponse(json.dumps(context), safe=False)

def readHotelReservation(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
    
    resultList = []

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom
                FROM RESERVASI_HOTEL
            """)

            result = cursor.fetchall()

            for hotelReservation in result:
                print(hotelReservation[1] > datetime.now().date())
                resultList.append(
                    [hotelReservation[0],
                     hotelReservation[1], 
                     hotelReservation[2], 
                     hotelReservation[3], 
                     hotelReservation[4],
                     hotelReservation[1] > datetime.now().date()])

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    
    return render(request, "hotel/readHotelReservation.html", {'listHotelReservation' : resultList})

def updateHotelReservation(request, param1, param2):
    context = []

    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    UPDATE RESERVASI_HOTEL
                    SET tglkeluar = '{request.POST['tglkeluar']}'
                    WHERE (kodepasien, tglmasuk) = ('{request.POST['kodepasien']}', '{request.POST['tglmasuk']}')
                """)

                return redirect("hotel:readHotelReservation")
            except:
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT kodepasien, tglmasuk, tglkeluar, kodehotel, koderoom
                FROM RESERVASI_HOTEL
                WHERE (kodepasien, tglmasuk) = ('{param1}', '{param2}')
            """)

            result = cursor.fetchall()[0]
            context = {'hotelReservationDetail' : result}

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "hotel/updateHotelReservation.html", context)

def deleteHotelReservation(request, param1, param2):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT idtransaksibooking FROM TRANSAKSI_BOOKING
                WHERE (kodepasien, tglmasuk) = ('{param1}', '{param2}')
            """)

            idtransaksi = cursor.fetchone()[0]

            cursor.execute(f"""
                DELETE FROM TRANSAKSI_BOOKING
                WHERE (kodepasien, tglmasuk) = ('{param1}', '{param2}')
            """)

            cursor.execute(f"""
                DELETE FROM TRANSAKSI_HOTEL
                WHERE idtransaksi = '{idtransaksi}'
            """)

            cursor.execute(f"""
                DELETE FROM RESERVASI_HOTEL
                WHERE (kodepasien, tglmasuk) = ('{param1}', '{param2}')
            """)

            return redirect("hotel:readHotelReservation")
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
        
    return redirect("hotel:readHotelReservation")

def readTransaksiHotel(request):
    if request.session.get("peran") != "admin satgas":
        return redirect("pengguna:index")
    
    resultList = []

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar
                FROM TRANSAKSI_HOTEL
            """)

            result = cursor.fetchall()

            for transaksiHotel in result:
                resultList.append(
                    [transaksiHotel[0],
                     transaksiHotel[1], 
                     transaksiHotel[2] if transaksiHotel[2] != None else '-', 
                     transaksiHotel[3] if transaksiHotel[3] != None else '-', 
                     transaksiHotel[4],
                     transaksiHotel[5]])

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    
    return render(request, "hotel/readTransaksiHotel.html", {'listTransaksiHotel' : resultList})

def updateTransaksiHotel(request, pk):
    context = []

    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    UPDATE TRANSAKSI_HOTEL
                    SET statusbayar = '{request.POST['statusbayar']}'
                    WHERE idtransaksi = '{pk}'
                """)

                return redirect("hotel:readTransaksiHotel")
            except:
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT idtransaksi, kodepasien, tanggalpembayaran, waktupembayaran, totalbayar, statusbayar
                FROM TRANSAKSI_HOTEL
                WHERE idtransaksi = '{pk}'
            """)

            result = cursor.fetchall()[0]
            context = {'hotelTransaksiDetail' : result}

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "hotel/updateTransaksiHotel.html", context)

def readTransaksiBooking(request):
    if request.session.get("peran") not in ("admin satgas", "pengguna publik"):
        return redirect("pengguna:index")
    
    resultList = []

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT idtransaksibooking, totalbayar, kodepasien, tglmasuk
                FROM TRANSAKSI_BOOKING
            """)

            result = cursor.fetchall()

            for transaksiBooking in result:
                resultList.append(
                    [transaksiBooking[0],
                     transaksiBooking[1], 
                     transaksiBooking[2], 
                     transaksiBooking[3]])

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    
    return render(request, "hotel/readTransaksiBooking.html", {'listTransaksiBooking' : resultList})
