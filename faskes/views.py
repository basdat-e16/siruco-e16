from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.db.utils import IntegrityError, InternalError
from django.http import JsonResponse
import traceback
import json
from django.views.decorators.csrf import csrf_exempt

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]

def read(request):
    query = "SELECT * from faskes"
    with connection.cursor() as cursor:
        cursor.execute(query)
        data = dictfetchall(cursor)
    return render(request, 'faskes/read.html', {'data':data})

def detail(request, kode):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * from faskes WHERE kode=%s", [kode])
        res = dictfetchall(cursor)

    dataGet = {
        'kode' : res[0]['kode'],
        'tipe' : res[0]['tipe'],
        'nama' : res[0]['nama'],
        'statusmilik' : res[0]['statusmilik'],
        'jalan' : res[0]['jalan'],
        'kelurahan' : res[0]['kelurahan'],
        'kecamatan' : res[0]['kecamatan'],
        'kabkot' : res[0]['kabkot'],
        'prov' : res[0]['prov'],
    }
    return render(request, 'faskes/detail.html', {'data':dataGet})

@csrf_exempt
def create(request):
    if request.method == "POST":
        data = request.POST
        print(data)
        kode = str(data['kode'])
        tipe = str(data['tipe'])
        nama = str(data['nama'])
        statusmilik = str(data['statusmilik'])
        jalan = str(data['jalan'])
        kelurahan = str(data['kelurahan'])
        kecamatan = str(data['kecamatan'])
        kabkot = str(data['kabkot'])
        prov = str(data['prov'])

        
        with connection.cursor() as cursor:
            cursor.execute("INSERT INTO faskes VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s);", 
            [kode, tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov])

        return redirect("faskes:read")

    #tampilan form create
    username = request.session['username']
    with connection.cursor() as cursor:
        cursor.execute("SELECT kode FROM FASKES ORDER BY kode DESC LIMIT 1")
        res = cursor.fetchone()

    temp_kode = str(int(res[0]) + 1)
    if(len(temp_kode) == 1):
        new_kode = "00"+ temp_kode
    elif(len(temp_kode) == 2):
        new_kode = "0"+ temp_kode
    else: 
        new_kode = temp_kode
    return render(request, "faskes/create.html", 
    {'data' : new_kode,
    'message' : "Buat Faskes"
    })
    

def update(request, kode):
    #tampilan form update
    with connection.cursor() as cursor:
        cursor.execute("Select * from faskes where kode = %s", [kode])
        res = dictfetchall(cursor)

    dataGet = {
        'kode' : res[0]['kode'],
        'tipe' : res[0]['tipe'],
        'nama' : res[0]['nama'],
        'statusmilik' : res[0]['statusmilik'],
        'jalan' : res[0]['jalan'],
        'kelurahan' : res[0]['kelurahan'],
        'kecamatan' : res[0]['kecamatan'],
        'kabkot' : res[0]['kabkot'],
        'prov' : res[0]['prov'],
    }
    print(dataGet)
    return render(request, 'faskes/update.html', {'data': dataGet})

@csrf_exempt
def updatePost(request):
    if request.method == "POST":
        data = request.POST
        print(data)
        kode = str(data['kode'])
        tipe = str(data['tipe'])
        nama = str(data['nama'])
        statusmilik = str(data['statusmilik'])
        jalan = str(data['jalan'])
        kelurahan = str(data['kelurahan'])
        kecamatan = str(data['kecamatan'])
        kabkot = str(data['kabkot'])
        prov = str(data['prov'])
        
        with connection.cursor() as cursor:
            cursor.execute("""UPDATE FASKES set 
            tipe= %s,
            nama= %s,
            statusmilik= %s,
            jalan= %s,
            kelurahan= %s,
            kecamatan= %s,
            kabkot= %s,
            prov= %s where kode = %s""",
            [tipe, nama, statusmilik, jalan, kelurahan, kecamatan, kabkot, prov, kode])
            
        return redirect("faskes:read")


def delete(request, kode):
    with connection.cursor() as cursor:
        cursor.execute("DELETE FROM FASKES WHERE kode=%s", [kode])
        return redirect("faskes:read")