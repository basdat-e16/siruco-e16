from django.urls import path

from . import views

app_name = 'faskes'

urlpatterns = [
    path('', views.read, name='read'),
    path('create/', views.create, name='create'),
    path('detail/<str:kode>', views.detail, name='detail'),
    path('update/<str:kode>', views.update, name='update'),
    path('updatePost/', views.updatePost, name='updatePost'),
    path('delete/<str:kode>', views.delete, name='delete'),
]