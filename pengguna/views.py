from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.db.utils import IntegrityError, InternalError
import traceback

# Create your views here.
def index(request):
    return render(request, "pengguna/index.html")

def loginview(request):
    if request.session.get("username", False):
        return redirect("pengguna:index")
    if request.method == "POST":
        found = False
        role = None

        with connection.cursor() as cursor:

            # ini buat execute query database
            cursor.execute(f"""
                SELECT * FROM AKUN_PENGGUNA
                WHERE username='{request.POST["username"]}'
                AND password='{request.POST["password"]}'
                """)

            row = cursor.fetchall() # ini dibalikin dalem bentuk multi-dimensional array
            if (len(row) != 0): # Berhasil login

                # contohnya buat set session.username menjadi row pertama (index ke 0) kolom pertama (index ke 0)
                # dalem kasus gw, kolom username adalah kolom paling awal di table PENGGUNA
                request.session["username"] = row[0][0]
                request.session["password"] = row[0][1] 
                request.session["peran"] = row[0][2] 
                return redirect("pengguna:index")

            else: # Gagal login
                messages.add_message(request, messages.WARNING, "Maaf, username atau password salah.")

    return render(request, "pengguna/login.html")

def registerview(request):
    if request.session.get("username", False):
        return redirect("pengguna:index")

    if request.method == "POST":
        username = request.POST['username']
        peran = request.POST['peran']
        password = request.POST['password']

        print(request.POST['username'])
        print(request.POST['peran'])
        print(request.POST['password'])

        with connection.cursor() as cursor:
            # try:
            #     cursor.execute(f"""
            #     INSERT INTO PENGGUNA VALUES
            #     ('{username}', '{name}', '{password}')
            #     """)
            #     messages.add_message(request, messages.success, f"Registrasi Berhasil, silakan login")
            #     return redirect("pengguna:login")
            # except:
            #     messages.add_message(request, messages.warning, f"Something bad happened")

            try:
                cursor.execute(f"""
                    SELECT FROM AKUN_PENGGUNA WHERE username = '{username}'
                """)

                # kalo engga ada
                if len(cursor.fetchall()) == 0:
                    cursor.execute(f"""
                        INSERT INTO AKUN_PENGGUNA (username, password, peran) VALUES
                        ('{username}', '{password}', '{peran}')
                    """)
                
                if peran == "admin sistem":
                    cursor.execute(f"""
                        INSERT INTO ADMIN (username) VALUES ('{username}')
                    """)
                
                elif peran == "pengguna publik":
                    cursor.execute(f"""
                        INSERT INTO PENGGUNA_PUBLIK (username, nik, nama, status, peran, nohp) VALUES
                        ('{username}', '{request.POST['nik']}', '{request.POST['nama']}', 'AKTIF', '{peran}', '{request.POST['nohp']}')    
                    """)
                
                elif peran == "dokter":
                    cursor.execute(f"""
                        INSERT INTO ADMIN (username) VALUES ('{username}')
                    """)

                    cursor.execute(f"""
                        INSERT INTO DOKTER (nostr, username, nama, nohp, gelardepan, gelarbelakang) VALUES
                        ('{request.POST['nostr']}', '{username}', '{request.POST['nama']}', '{request.POST['nohp']}', '{request.POST['gelardepan']}', '{request.POST['gelarbelakang']}')    
                    """)
                
                elif peran == "admin satgas":
                    cursor.execute(f"""
                        INSERT INTO ADMIN (username) VALUES ('{username}')
                    """)
                    
                    cursor.execute(f"""
                        INSERT INTO ADMIN_SATGAS (username, idfaskes) VALUES
                        ('{username}', '{request.POST['idfaskes']}')
                    """)
                
                messages.add_message(request, messages.SUCCESS, f"Registrasi berhasil, silahkan login")
                messages.add_message(request, messages.SUCCESS, f"{request.POST['username']}")

                # return render(request, "pengguna/register.html")

            except InternalError:
                messages.add_message(request, messages.WARNING, f'''Password Anda belum memenuhi syarat, silahkan 
                pastikan bahwa password minimal terdapat 1 huruf kapital dan 1 angka''')
            
            except IntegrityError:
                messages.add_message(request, messages.WARNING, f"Username {request.POST['username']} sudah ada")
                traceback.print_exc()
            except:
                traceback.print_exc()
                #print(request.POST.get("kode", False))
                messages.add_message(request, messages.WARNING, f"Ada gangguan server internal, mohon coba lagi")

    with connection.cursor() as cursor:
        try:
            context = []
            cursor.execute(f"""
                SELECT kode FROM FASKES
            """)
            
            idfaskes = cursor.fetchall()

            for i in range(len(idfaskes)):
                context.append(idfaskes[i][0])
        except:
            pass

    return render(request, "pengguna/register.html", {'idfaskes' : context})

def logoutview(request):
    # buat ilangin variabel dari session
    request.session.pop("username", None)
    return redirect("pengguna:index")
