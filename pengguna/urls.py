from django.urls import path

from . import views

app_name = 'pengguna'

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', views.loginview, name='login'),
    path('register/', views.registerview, name='register'),
    path("logout/", views.logoutview, name="logout"),
]