from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def index(request):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")
    
    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from pengguna_publik
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            public_user = True

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            admin_satgas = True

    if (not (public_user or admin_satgas)):
        return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select tm.idtransaksi, tm.idtransaksimakan, tm.totalbayar, th.statusbayar 
            from transaksi_makan tm
            inner join transaksi_hotel th
            on tm.idtransaksi = th.idtransaksi
            order by tm.idtransaksi asc, tm.idtransaksimakan asc;
        """)

        row = cursor.fetchall()
        
        transaksi_makan_query = [
            (no, item[0], item[1], item[2], item[3]) for (no, item)
            in enumerate(row, start=1)
        ]
    
            
        print(transaksi_makan_query)
        context = {
            "transaksi_makan_query" : transaksi_makan_query,  
            "admin_satgas" : admin_satgas,
            "public_user" : public_user,
        }

    return render(request, "transaksi_makan/index.html", context=context)

def detail_transaksi(request, id_transaksi, id_transaksi_makan):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from pengguna_publik
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            public_user = True

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            admin_satgas = True

    if (not (admin_satgas or public_user)):
        return redirect("transaksi_makan:index")
    

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select rh.kodehotel
            from transaksi_makan tm
            inner join transaksi_hotel th
            on tm.idtransaksi = th.idtransaksi
            inner join transaksi_booking tb
            on tb.idtransaksibooking = th.idtransaksi
            inner join reservasi_hotel rh
            on rh.kodepasien = tb.kodepasien and rh.tglmasuk = tb.tglmasuk
            where tm.idtransaksi = '{id_transaksi}' and tm.idtransaksimakan = '{id_transaksi_makan}';
                """)

        row = cursor.fetchall()
        kode_hotel = row[0][0]

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select tm.totalbayar
            from transaksi_makan tm
            where tm.idtransaksi = '{id_transaksi}' and tm.idtransaksimakan = '{id_transaksi_makan}';
                """)

        row = cursor.fetchall()
        total_bayar = row[0][0]

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select dp.id_pesanan, dp.kodepaket, pm.harga
            from transaksi_makan tm
            inner join daftar_pesan dp
            on tm.idtransaksi = dp.id_transaksi and tm.idtransaksimakan = dp.idtransaksimakan
            inner join paket_makan pm
            on pm.kodehotel = dp.kodehotel and pm.kodepaket = dp.kodepaket
            where tm.idtransaksi = '{id_transaksi}' and tm.idtransaksimakan = '{id_transaksi_makan}'
            order by dp.id_pesanan asc;
            """)

        row = cursor.fetchall()

        paket_makan_query = [
            (item[0], item[1], item[2]) for (no, item)
            in enumerate(row, start=1)
        ]

    context = {
        "id_transaksi" : id_transaksi,
        "id_transaksi_makan": id_transaksi_makan,
        "kode_hotel" : kode_hotel, 
        "paket_makan_query" : paket_makan_query, 
        "total_bayar" : total_bayar, 
    }

    # some error occured
    return render(request, "transaksi_makan/detail.html", context=context)

def create_transaksi(request):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from pengguna_publik
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            public_user = True

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            admin_satgas = True

    if (not (admin_satgas or public_user)):
        return redirect("transaksi_makan:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            SELECT idtransaksi
            FROM TRANSAKSI_HOTEL
            order by idtransaksi asc;
        """)
        row = cursor.fetchall()
        id_transaksi_query = [
            (item[0]) for (no, item)
            in enumerate(row, start=1)
        ]

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select idtransaksimakan
            from transaksi_makan
            order by idtransaksimakan desc
            limit 1;
        """)
        row = cursor.fetchall()
        max_id = row[0][0]
        max_id = max_id.split("ZCZC-MK-")[1]
        max_id = int(max_id)
        max_id += 1
        if max_id < 10:
            max_id = str('ZCZC-MK-0' + str(max_id))
        else:
            max_id = str('ZCZC-MK-' + str(max_id))
    
    context = {
        "id_transaksi_query" : id_transaksi_query,  
        "max_id" : max_id,
    }
    return render(request, "transaksi_makan/create.html", context=context)

@csrf_exempt
def get_kode_hotel(request):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from pengguna_publik
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            public_user = True

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            admin_satgas = True

    if (not (admin_satgas or public_user)):
        return redirect("transaksi_makan:index")

    if request.is_ajax and request.method == "POST":
        idtransaksi = json.loads(request.body.decode('utf-8'))
        idtransaksi = idtransaksi.get('idtransaksi')
        print(idtransaksi)
        with connection.cursor() as cursor:
            cursor.execute(f"""
                select rh.kodehotel
                from transaksi_hotel tr
                natural join reservasi_hotel rh
                where tr.idtransaksi = '{idtransaksi}';
            """)
            row = cursor.fetchall()
            kode_hotel = row[0][0]

        with connection.cursor() as cursor:
            cursor.execute(f"""
                select distinct pm.kodepaket
                from transaksi_hotel th
                inner join transaksi_booking tb
                on tb.idtransaksibooking = th.idtransaksi
                inner join reservasi_hotel rh
                on rh.kodepasien = tb.kodepasien and rh.tglmasuk = tb.tglmasuk
                inner join paket_makan pm
                on pm.kodehotel = rh.kodehotel
                where rh.kodehotel = '{kode_hotel}'
                order by pm.kodepaket asc;
            """)
            row = cursor.fetchall()
            paket_makan_query = [
                (item[0]) for (no, item)
                in enumerate(row, start=1)
            ]
    

        context = {
            "kode_hotel" : kode_hotel,
            "paket_makan_query" : paket_makan_query,  
        }
        return JsonResponse({"data":context}, status = 200)

@csrf_exempt
def submit_create_transaksi(request):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from pengguna_publik
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            public_user = True

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            admin_satgas = True

    if (not (admin_satgas or public_user)):
        return redirect("transaksi_makan:index")

    if request.is_ajax and request.method == "POST":
        data = json.loads(request.body.decode('utf-8'))
        print(data)
        id_transaksi = data.get("idTransaksi")
        id_transaksi_makan = data.get("idTransaksiMakan")
        kode_hotel = data.get("kodeHotel")
        list_pesanan = data.get("listPesanan")

        with connection.cursor() as cursor:
            cursor.execute(f"""
                INSERT INTO TRANSAKSI_MAKAN(IdTransaksi, IdTransaksiMakan, TotalBayar) VALUES
                ('{id_transaksi}', '{id_transaksi_makan}', 0);
            """)

        with connection.cursor() as cursor:
            count = 1
            for i in list_pesanan:
                cursor.execute(f"""
                    INSERT INTO DAFTAR_PESAN(IdTransaksiMakan, id_pesanan, Id_transaksi, KodeHotel, KodePaket) VALUES
                    ('{id_transaksi_makan}', {count} ,'{id_transaksi}', '{kode_hotel}','{i}');
                """)
                count += 1
        
        return JsonResponse({}, status = 200)

@csrf_exempt
def update_transaksi(request, id_transaksi, id_transaksi_makan):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")
            
    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("transaksi_makan:index")

    with connection.cursor() as cursor:
            cursor.execute(f"""
                select rh.kodehotel
                from transaksi_hotel tr
                natural join reservasi_hotel rh
                where tr.idtransaksi = '{id_transaksi}';
            """)
            row = cursor.fetchall()
            kode_hotel = row[0][0]

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select dp.kodepaket
            from transaksi_makan tm
            inner join daftar_pesan dp
            on tm.idtransaksi = dp.id_transaksi and tm.idtransaksimakan = dp.idtransaksimakan
            inner join paket_makan pm
            on pm.kodehotel = dp.kodehotel and pm.kodepaket = dp.kodepaket
            where tm.idtransaksi = '{id_transaksi}' and tm.idtransaksimakan = '{id_transaksi_makan}'
            order by dp.kodepaket asc;
                """)

        row = cursor.fetchall()

        pesanan_query = [
            (item[0]) for (no, item)
            in enumerate(row, start=1)
        ]

    with connection.cursor() as cursor:
            cursor.execute(f"""
                select distinct pm.kodepaket
                from transaksi_hotel th
                inner join transaksi_booking tb
                on tb.idtransaksibooking = th.idtransaksi
                inner join reservasi_hotel rh
                on rh.kodepasien = tb.kodepasien and rh.tglmasuk = tb.tglmasuk
                inner join paket_makan pm
                on pm.kodehotel = rh.kodehotel
                where rh.kodehotel = '{kode_hotel}'
                order by pm.kodepaket asc;
            """)
            row = cursor.fetchall()
            paket_makan_query = [
                (item[0]) for (no, item)
                in enumerate(row, start=1)
            ]

    context = {
        "id_transaksi" : id_transaksi,  
        "id_transaksi_makan" : id_transaksi_makan,
        "kode_hotel" : kode_hotel,
        "paket_makan_query" : paket_makan_query,
        "pesanan_query" : pesanan_query,

    }
    return render(request, "transaksi_makan/update.html", context=context)

@csrf_exempt
def submit_update_transaksi(request):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("transaksi_makan:index")

    if request.is_ajax and request.method == "POST":
        data = json.loads(request.body.decode('utf-8'))
        print(data)
        id_transaksi = data.get("idTransaksi")
        id_transaksi_makan = data.get("idTransaksiMakan")
        kode_hotel = data.get("kodeHotel")
        list_pesanan = data.get("listPesanan")
        with connection.cursor() as cursor:
                cursor.execute(f"""
                select id_pesanan
                from daftar_pesan
                where idtransaksimakan = '{id_transaksi_makan}' and id_transaksi = '{id_transaksi}'
                order by id_pesanan desc
                limit 1;
                """)
                row = cursor.fetchall()
                max_id = int(row[0][0]) + 1
        with connection.cursor() as cursor:
            for i in list_pesanan:
                cursor.execute(f"""
                    INSERT INTO DAFTAR_PESAN(IdTransaksiMakan, id_pesanan, Id_transaksi, KodeHotel, KodePaket) VALUES
                    ('{id_transaksi_makan}', {max_id},'{id_transaksi}', '{kode_hotel}','{i}');
                """)
                max_id += 1
        
        return JsonResponse({}, status = 200)

@csrf_exempt
def delete_transaksi(request, id_transaksi, id_transaksi_makan):
    public_user = False
    admin_satgas = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("transaksi_makan:index")

    if request.is_ajax and request.method == "DELETE":
        with connection.cursor() as cursor:
                cursor.execute(f"""
                DELETE FROM daftar_pesan
                WHERE id_transaksi = '{id_transaksi}' 
                and idtransaksimakan = '{id_transaksi_makan}';
                """)
        with connection.cursor() as cursor:
                cursor.execute(f"""
                DELETE FROM transaksi_makan
                WHERE idtransaksi = '{id_transaksi}' 
                and idtransaksimakan = '{id_transaksi_makan}';
                """)
        return JsonResponse({}, status = 200)

