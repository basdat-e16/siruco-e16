from django.urls import path

from . import views

app_name = 'transaksi_makan'

urlpatterns = [
    path('', views.index, name='index'),
    path("detail/<str:id_transaksi>/<str:id_transaksi_makan>/", views.detail_transaksi,name="detail_transaksi"),
    path("create/", views.create_transaksi,name="create_transaksi"),
    path("get-kode-hotel/", views.get_kode_hotel, name="get_kode_hotel"),
    path("create/submit/", views.submit_create_transaksi, name="submit_create_transaksi"),
    path("update/<str:id_transaksi>/<str:id_transaksi_makan>/", views.update_transaksi,name="update_transaksi"),
    path("update/submit/", views.submit_update_transaksi,name="submit_update_transaksi"),
    path("delete/<str:id_transaksi>/<str:id_transaksi_makan>/", views.delete_transaksi,name="delete_transaksi"),

]