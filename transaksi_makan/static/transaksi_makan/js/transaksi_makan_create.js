var listPaket = []

$(document).on('change', "#idT", function () {
    var json = {}
    json["idtransaksi"] = this.value;
    json = JSON.stringify(json);
    console.log(json);
    $.ajax({
        type: 'POST',
        url: "/transaksi-makan/get-kode-hotel/",
        data: json,
        success: function (data) {
            listPaket = data.data.paket_makan_query
            $("#kode-hotel").html(data.data.kode_hotel);
            $("#list-pesanan").html("");
            
        },
        Error: function(data){
            console.log("error");
        },
        fail: function(data){
            console.log("fail");
        }
    });

});

$(document).on('click', "#add-pesanan", function () {

    var ele = `
    <div>Kode Paket : 
    <select name="id-transaksi" class="idT">
        <option value="">Pilih Paket</option>`;

    $.each( listPaket, function( key, value ) {
        ele += `<option class="pesanan" value="${value}">${value}</option>`
    });
    ele += `
    </select>
    <label class="delete-pesanan"> X </label>
    </div>
    `;
    $("#list-pesanan").append(ele);
})

$(document).on('click', ".delete-pesanan", function () {
    $(this).parent().remove();   
})

$(document).on('click', "#submit", function () {
    var json = {}
    var idTransaksi = $('#idT').find(":selected").text();
    var idTransaksiMakan = $('#idTM').text();
    var kodeHotel = $('#kode-hotel').text();
    var listPesanan = $('#list-pesanan').find(".idT");
    var listSelectedPesanan = []
    console.log(listPesanan)
    $.each( listPesanan, function( key, value ) {
        var selected = $(value).find(":selected").text()
        console.log(selected);
        if (selected != "Pilih Paket") {
            listSelectedPesanan.push(selected)
        }
    });
    if (listSelectedPesanan.length == 0 ){
        alert("Pesanan tidak boleh kosong!");
        return false;
    }
    json['idTransaksi'] = idTransaksi
    json['idTransaksiMakan'] = idTransaksiMakan
    json['kodeHotel'] = kodeHotel
    json['listPesanan'] = listSelectedPesanan
    json = JSON.stringify(json);
    console.log(json);

    $.ajax({
        type: 'POST',
        url: "/transaksi-makan/create/submit/",
        data: json,
        success: function (data) {
            window.location = `/transaksi-makan/`
        },
        Error: function(data){
            console.log("error");
        },
        fail: function(data){
            console.log("fail");
        }
    });
})
