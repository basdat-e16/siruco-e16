

$(document).on('click', "#add-pesanan", function () {
    var selectionPesanan = $("#paket").find("option")
    var ele = `
    <div>Kode Paket: 
    <select name="paket" class="paket">`
    $.each( selectionPesanan, function( key, value ) {
        ele += `<option value="${value.text}">${value.text}</option>`
    });
    ele += `
    </select>
    <p class="delete-pesanan"> X </p>
    </div>
    `;
    $("#list-tambah-pesanan").append(ele);
})

$(document).on('click', ".delete-pesanan", function () {
    $(this).parent().remove();   
})

$(document).on('click', ".delete-pesanan", function () {
    $(this).parent().remove();   
})

$(document).on('click', "#submit", function () {
    var json = {}
    var idTransaksi = $('#idT').text();
    var idTransaksiMakan = $('#idTM').text();
    var kodeHotel = $('#kode-hotel').text();
    var listPesanan = $('#list-tambah-pesanan').find(".paket");
    var listSelectedPesanan = []
    console.log(listPesanan)
    $.each( listPesanan, function( key, value ) {
        var selected = $(value).find(":selected").text()
        console.log(selected);
        if (selected != "Pilih Paket") {
            listSelectedPesanan.push(selected)
        }
    });
    if (listSelectedPesanan.length == 0 ){
        alert("Pesanan tidak boleh kosong!");
        return false;
    }
    json['idTransaksi'] = idTransaksi
    json['idTransaksiMakan'] = idTransaksiMakan
    json['kodeHotel'] = kodeHotel
    json['listPesanan'] = listSelectedPesanan
    json = JSON.stringify(json);
    console.log(json);

    
    $.ajax({
        type: 'POST',
        url: "/transaksi-makan/update/submit/",
        data: json,
        success: function (data) {
            window.location = `/transaksi-makan/`
        },
        Error: function(data){
            console.log("error");
        },
        fail: function(data){
            console.log("fail");
        }
    });
    
})