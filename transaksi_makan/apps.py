from django.apps import AppConfig


class TransaksiMakanConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'transaksi_makan'
