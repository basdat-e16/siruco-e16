from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.db.utils import IntegrityError, InternalError
import traceback
from datetime import datetime


# Create your views here.

def createAppointment(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
        
    resultList = []

    
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
            SELECT nostr,username,kode_faskes,shift,tanggal FROM JADWAL_DOKTER
            """)
            result =cursor.fetchall()

                # messages.add_message(request, messages.SUCCESS, f"Appointment telah dibuat")
        except InternalError:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
        except :
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
        
        return render(request, "appointment/createAppointment.html",{"listJadwalAppointment" : result})
    


def formAppointment(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")

    
    with connection.cursor() as cursor:
        try:
            tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
            tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
            cursor.execute(f"""
            SELECT nostr,username,kode_faskes,shift,tanggal FROM JADWAL_DOKTER
            WHERE nostr='{request.POST['nostr']}' AND username='{request.POST['username']}' AND kode_faskes='{request.POST['kodefaskes']}'
                AND shift='{request.POST['shift']}' AND tanggal='{tanggal}'
            """)
            result =cursor.fetchall()
            if request.method == "POST":
                 if request.session.get("peran") == "pengguna publik":
                    cursor.execute(f"""
                    SELECT nik FROM PASIEN
                    WHERE idpendaftar = '{request.session['username']}'
                    """)
                    nik_pasien = cursor.fetchall()

                 elif request.session.get("peran") == "admin satgas":
                    cursor.execute(f"""
                    SELECT nostr,username,kode_faskes,shift,tanggal FROM JADWAL_DOKTER
                    """)
                    nik_pasien = cursor.fetchall()

            return render(request, "appointment/formAppointment.html",{"nik_pasien" : nik_pasien,"listJadwalAppointment" : result})
                
                # messages.add_message(request, messages.SUCCESS, f"Appointment telah dibuat")
        except InternalError:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
        except :
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
        
        return render(request, "appointment/formAppointment.html")

def saveAppointment(request):
    with connection.cursor() as cursor:
        try:
            tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
            tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
            cursor.execute(f"""
                INSERT INTO MEMERIKSA
                (nik_pasien,nostr,username_dokter,kode_faskes, praktek_shift, 
                praktek_tgl) VALUES
                ('{request.POST['nik_pasien']}','{request.POST['nostr']}','{request.POST['username']}','{request.POST['kodefaskes']}', '{request.POST['shift']}',
                '{tanggal}')
            """)
            return redirect("appointment:readAppointment")
        
        except InternalError:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
            return createAppointment(request)

def readAppointment(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
    
    with connection.cursor() as cursor:
        try:
            resultList = []
            result=[]
            if request.session.get("peran") == "pengguna publik":
                cursor.execute(f"""
                SELECT A.nik_pasien,A.nostr,A.username_dokter, A.kode_faskes, A.praktek_shift,A.praktek_tgl,A.rekomendasi 
                FROM MEMERIKSA A, PASIEN B, PENGGUNA_PUBLIK C 
                WHERE B.idpendaftar = C.username AND A.nik_pasien = B.nik AND C.username = '{request.session.get("username")}'
                """)

                result = cursor.fetchall()
            
            elif request.session.get("peran") == "dokter":
                cursor.execute(f"""
                SELECT A.nik_pasien,A.nostr,A.username_dokter, A.kode_faskes, A.praktek_shift,A.praktek_tgl,A.rekomendasi 
                FROM MEMERIKSA A
                WHERE A.username_dokter = '{request.session.get("username")}'
                """)

                result = cursor.fetchall()
            
            elif request.session.get("peran") =="admin satgas":
                cursor.execute(f"""
                SELECT A.nik_pasien,A.nostr,A.username_dokter, A.kode_faskes, A.praktek_shift,A.praktek_tgl,A.rekomendasi 
                FROM MEMERIKSA A
                """)

                result = cursor.fetchall()

            for jadwal_periksa in result:
                resultList.append([jadwal_periksa[0], jadwal_periksa[1],jadwal_periksa[2],jadwal_periksa[3],jadwal_periksa[4],jadwal_periksa[5],jadwal_periksa[6]])
            
            return render(request, "appointment/readAppointment.html", {"listJadwalPeriksa" : resultList})

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "appointment/readAppointment.html")


def updateAppointment(request):

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT nik_pasien, username_dokter, praktek_shift, praktek_tgl,kode_faskes,rekomendasi
                FROM MEMERIKSA WHERE nik_pasien = '{request.POST['nik']}'
            """)

            result = cursor.fetchall()
            # print(type(result))
            # print(result)
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
            print("exec happened")

    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
                tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
                cursor.execute(f"""
                    SELECT * FROM MEMERIKSA
                    WHERE nik_pasien='{request.POST['nik']}' AND username_dokter='{request.POST['username']}'
                    AND kode_faskes='{request.POST['faskes']}' AND praktek_shift='{request.POST['shift']}'
                    AND praktek_tgl='{tanggal}'
                """)

                memeriksa = cursor.fetchall()
                return render(request, 'appointment/updateAppointment.html', {'updateAppointment':memeriksa})
            except:
                traceback.print_exc()
    
    return render(request, 'appointment/updateAppointment.html')

def saveUpdateAppointment(request):
    if request.method == 'POST':
        with connection.cursor() as cursor:
            try:
                tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
                tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
                cursor.execute(f"""
                    UPDATE MEMERIKSA
                    SET rekomendasi='{request.POST['rekomendasi']}'
                    WHERE nik_pasien='{request.POST['nik']}' AND username_dokter='{request.POST['username']}'
                    AND kode_faskes='{request.POST['faskes']}' AND praktek_shift='{request.POST['shift']}'
                    AND praktek_tgl='{tanggal}'
                """)
            except:
                traceback.print_exc()
    
    return redirect('appointment:readAppointment')


def deleteAppointment(request):
    if request.session.get('peran') != 'admin satgas':
        return redirect('main:home')
    else:
        with connection.cursor() as cursor:
            try:
                tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
                tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
                cursor.execute(f"""
                    DELETE FROM MEMERIKSA
                    WHERE nik_pasien='{request.POST['nik']}' AND username_dokter='{request.POST['username']}'
                    AND kode_faskes='{request.POST['faskes']}' AND praktek_shift='{request.POST['shift']}'
                    AND praktek_tgl='{tanggal}'
                """)
            except:
                traceback.print_exc()
                messages.add_message(request, messages.WARNING, f"""Ada gangguan server, harap coba lagi""")
        return redirect('appointment:readAppointment')
