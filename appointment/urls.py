from django.urls import path

from . import views

app_name = 'appointment'

urlpatterns = [
    path('createAppointment/', views.createAppointment, name='createAppointment'),
    path('formAppointment/', views.formAppointment, name='formAppointment'),
    path('readAppointment/', views.readAppointment, name='readAppointment'),
    path("saveAppointment/", views.saveAppointment, name="saveAppointment"),
    path('updateAppointment/', views.updateAppointment, name='updateAppointment'),
    path('saveUpdateAppointment/', views.saveUpdateAppointment, name='saveUpdateAppointment'),
    path('deleteAppointment/', views.deleteAppointment, name='deleteAppointment'),
]