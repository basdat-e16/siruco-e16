from django.urls import path

from . import views

app_name = 'paket_makan'

urlpatterns = [
    path('', views.index, name='index'),
    path("create/", views.create_paket,name="create_paket"),
    path("create/submit/", views.submit_create_paket, name="submit_create_paket"),
    path("update/<str:kode_hotel>/<str:kode_paket>/", views.update_paket,name="update_paket"),
    path("update/submit/", views.submit_update_paket,name="submit_update_paket"),
    path("delete/<str:kode_hotel>/<str:kode_paket>/", views.delete_paket,name="delete_paket"),

]