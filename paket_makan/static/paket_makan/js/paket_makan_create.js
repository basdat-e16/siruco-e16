$(document).on('click', "#submit", function () {
    var json = {}
    var kodeHotel = $('#kode-hotel').find(":selected").text();
    var kodePaket = $('#kode-paket').val();
    console.log(kodePaket)
    var namaPaket = $('#nama-paket').val();
    var harga = $('#harga').val();

    if (kodeHotel == "Pilih Kode" ){
        alert("Kode hotel kosong!");
        return false;
    }
    if (kodePaket.length == 0 ){
        alert("Kode paket kosong!");
        return false;
    }
    if (namaPaket.length == 0 ){
        alert("Nama paket kosong!");
        return false;
    }
    if (harga.length == 0 ){
        alert("Harga kosong!");
        return false;
    }
    json['kodeHotel'] = kodeHotel
    json['kodePaket'] = kodePaket
    json['namaPaket'] = namaPaket
    json['harga'] = harga
    json = JSON.stringify(json);
    console.log(json);

    $.ajax({
        type: 'POST',
        url: "/paket-makan/create/submit/",
        data: json,
        success: function (data) {
            window.location = `/paket-makan/`
        },
        Error: function(data){
            console.log("error");
        },
        fail: function(data){
            console.log("fail");
        }
    });
})
