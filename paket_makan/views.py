from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.http import JsonResponse
import json
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
def index(request):
    admin_sistem = False
    public_dan_satgas = False


    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")
    
    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from pengguna_publik
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            public_dan_satgas = True

        
    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from admin_satgas
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            public_dan_satgas = True

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) != 0):
            admin_sistem = True

    if (not (admin_sistem or public_dan_satgas)):
        return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select pm.kodehotel, pm.kodepaket, pm.nama, pm.harga,
            CASE WHEN dp.kodehotel IS NOT NULL
                THEN 'true'
                ELSE 'false'
                END
            from paket_makan pm
            left join daftar_pesan dp
            on pm.kodehotel = dp.kodehotel 
            and pm.kodepaket = dp.kodepaket
            order by pm.kodehotel asc, pm.kodepaket asc;
        """)

        row = cursor.fetchall()
        
        paket_makan_query = [
            (no, item[0], item[1], item[2], item[3], item[4]) for (no, item)
            in enumerate(row, start=1)
        ]
    
        context = {
            "paket_makan_query" : paket_makan_query,  
            "admin_sistem" : admin_sistem,
            "public_and_satgas" : public_dan_satgas,
        }

    return render(request, "paket_makan/index.html", context=context)

def create_paket(request):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("paket_makan:index")


    with connection.cursor() as cursor:
        cursor.execute(f"""
            select kode
            from hotel
            order by kode asc;
        """)
        row = cursor.fetchall()
        kode_hotel_query = [
            (item[0]) for (no, item)
            in enumerate(row, start=1)
        ]
    context = {
        "kode_hotel_query" : kode_hotel_query,  
    }
    return render(request, "paket_makan/create.html", context=context)

@csrf_exempt
def submit_create_paket(request):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("paket_makan:index")

    if request.is_ajax and request.method == "POST":
        data = json.loads(request.body.decode('utf-8'))
        print(data)
        kode_hotel = data.get("kodeHotel")
        kode_paket = data.get("kodePaket")
        nama_paket = data.get("namaPaket")
        harga = data.get("harga")

        with connection.cursor() as cursor:
            cursor.execute(f"""
                INSERT INTO PAKET_MAKAN(KodeHotel, KodePaket, Nama, Harga) VALUES
                ('{kode_hotel}', '{kode_paket}', '{nama_paket}', {harga});
            """)
        return JsonResponse({}, status = 200)

@csrf_exempt
def update_paket(request, kode_hotel, kode_paket):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("paket_makan:index")

    with connection.cursor() as cursor:
            cursor.execute(f"""
                select * 
                from paket_makan
                where kodehotel = '{kode_hotel}' 
                and kodepaket = '{kode_paket}';
            """)
            row = cursor.fetchall()
            paket_makan = row[0]

    context = {
        "paket_makan" : paket_makan,
    }
    return render(request, "paket_makan/update.html", context=context)

@csrf_exempt
def submit_update_paket(request):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("paket_makan:index")

    if request.is_ajax and request.method == "POST":
        data = json.loads(request.body.decode('utf-8'))
        print(data)
        kode_hotel = data.get("kodeHotel")
        kode_paket = data.get("kodePaket")
        nama_paket = data.get("namaPaket")
        harga = data.get("harga")
        with connection.cursor() as cursor:
            cursor.execute(f"""
                UPDATE paket_makan
                SET nama = '{nama_paket}',
                harga = '{harga}'
                WHERE kodehotel = '{kode_hotel}' 
                and kodepaket = '{kode_paket}';
            """)
        return JsonResponse({}, status = 200)

@csrf_exempt
def delete_paket(request, kode_hotel, kode_paket):
    admin_sistem = False

    nama = request.session.get("username")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select * 
            from akun_pengguna
            where username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("pengguna:index")

    with connection.cursor() as cursor:
        cursor.execute(f"""
            select *
            from admin a
            where a.username not in 
            (
                select ads.username
                from admin_satgas ads
            ) and a.username = '{nama}';
        """)
        row = cursor.fetchall()
        if (len(row) == 0):
            return redirect("paket_makan:index")

    if request.is_ajax and request.method == "DELETE":
        with connection.cursor() as cursor:
                cursor.execute(f"""
                DELETE FROM paket_makan
                WHERE kodehotel = '{kode_hotel}' 
                and kodepaket = '{kode_paket}';
                """)
        return JsonResponse({}, status = 200)
