from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.db.utils import IntegrityError, InternalError
import traceback
from datetime import datetime

# Create your views here.

def readDoctorSchedule(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
    
    resultList = []
    
    with connection.cursor() as cursor:
        try:
            if request.session.get("peran") == "dokter":
                cursor.execute(f"""
                    SELECT * FROM JADWAL_DOKTER WHERE username = '{request.session.get("username")}'
                """)

            elif request.session.get("peran") == "admin satgas" or request.session.get("peran") == "pengguna publik":
                cursor.execute(f"""
                    SELECT * FROM JADWAL_DOKTER
                """)

            result = cursor.fetchall()

            for jadwal_dokter in result:
                resultList.append([jadwal_dokter[0], jadwal_dokter[1],jadwal_dokter[2],jadwal_dokter[3],jadwal_dokter[4],jadwal_dokter[5]])
            
        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "dokter/readDoctorSchedule.html", {"listJadwal" : resultList}) 

def createSchedule(request):
    if request.session.get("username") == None:
        return redirect("pengguna:login")
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    SELECT noSTR,Username FROM DOKTER WHERE username = '{request.session.get("username")}'
                """)
                result =cursor.fetchall()[0]
                tanggal = datetime.strptime(request.POST['tanggal'], '%b %d, %Y')
                tanggal = datetime.strftime(tanggal, '%d-%m-%Y')
                cursor.execute(f"""
                    INSERT INTO JADWAL_DOKTER
                    (nostr,username,kode_faskes, shift, 
                    tanggal,jmlPasien) VALUES
                    ('{result[0]}','{result[1]}','{request.POST['kodefaskes']}', '{request.POST['shift']}',
                    '{tanggal}','0')
                """)

                messages.add_message(request, messages.SUCCESS, f"Jadwal telah dibuat")

                return redirect("dokter:readDoctorSchedule")

            except InternalError:
                messages.add_message(request, messages.WARNING, "Terdapat internal error")
                traceback.print_exc()
            except :
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()
    

    with connection.cursor() as cursor:
        try:

            cursor.execute(f"""
            SELECT kode_faskes, shift, tanggal from JADWAL
            except (
                select kode_faskes, shift, tanggal from JADWAL_DOKTER
            )
            """)
            context= cursor.fetchall()
            print(context)
        except InternalError:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
        except :
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()

    return render(request, "dokter/createSchedule.html",{'context':context})


