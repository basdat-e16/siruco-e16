from django.urls import path

from . import views

app_name = 'dokter'

urlpatterns = [
    path('createSchedule/', views.createSchedule, name='createSchedule'),
    path('readDoctorSchedule/', views.readDoctorSchedule, name='readDoctorSchedule'),
]