$(document).ready(function(){
    console.log("hello early");
    $('#koderumahsakit').on('change', function(){
        var kode = this.value;
        console.log("change on koders " + kode);
        $.ajax({
            url:'/roomnbed/kodeRuanganBed?q=' + kode,
            success:function(data){
                var kode_room = JSON.parse(data);
                kode_room = kode_room['kodeRuangan'];
                console.log(kode_room);

                $('#koderuangan').empty();
                $('#koderuangan').append("<option disabled selected value> -- select an option -- </option>");

                for(var i = 0; i < kode_room.length; i++){
                    console.log(kode_room[i][0]);
                    $('#koderuangan').append(
                        '<option value="' + kode_room[i][0] + '">' + kode_room[i][0] + '</option>'
                    )
                }
            }
        })
    });

    console.log("hello");

    $('#koderuangan').on('change', function(){
        var koders = $('#koderumahsakit').val();
        var koderuangan = $('#koderuangan').val();
        $.ajax({
            url:'/roomnbed/kodeBed?rs='+koders+'&ruangan='+koderuangan,
            success:function(data){
                var kode_room = JSON.parse(data);
                kode_room = kode_room['kodeBed'];
                console.log("change on koderuangan " + kode_room);
                $('#kodebed').val(kode_room);
                $('#kodebed').attr("placeholder", kode_room);
            }
        })
    })
})  