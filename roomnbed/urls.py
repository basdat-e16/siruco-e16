from django.urls import path

from . import views

app_name = 'roomnbed'

urlpatterns = [
    path('createRuangan/', views.createRuangan, name='createRuangan'),
    path('kodeRuangan/', views.kodeRuangan, name='koderuangan'),
    path('createBed/', views.createBed, name='createBed'),
    path('kodeRuanganBed/', views.kodeRuanganBed, name='kodeRuanganBed'),
    path('kodeBed/', views.kodeBed, name='kodeBed'),
    path('readRuangan/', views.readRuangan, name='readRuangan'),
    path('readBed/', views.readBed, name='readBed'),
    path('updateRuangan/<param1>/<param2>', views.updateRuangan, name='updateRuangan'),
    path('deleteBed/<param1>/<param2>/<param3>/', views.deleteBed, name='deleteBed')
]