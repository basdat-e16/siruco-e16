from django.shortcuts import render, redirect
from django.db import connection
from django.contrib import messages
from django.db.utils import IntegrityError, InternalError
from django.http import JsonResponse
import traceback
import json
# Create your views here.

def createRuangan(request):
    
    if request.session.get("peran") != "admin satgas":
        return redirect("pengguna:index")
    
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    INSERT INTO RUANGAN_RS (koders, koderuangan, tipe, jmlbed, harga) VALUES
                    ('{request.POST['koderumahsakit']}', '{request.POST['koderuangan']}', '{request.POST['tipe']}', 0, '{request.POST['harga']}')
                """)

                return redirect("roomnbed:readRuangan")

            except:
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()
                


    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT DISTINCT kode_faskes FROM RUMAH_SAKIT
            """)

            result = cursor.fetchall()

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "roomnbed/createRuangan.html", {'context':result})

def kodeRuangan(request):
    kodeRs = request.GET['q']
    kodeRuangan = ""

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT DISTINCT koderuangan FROM RUANGAN_RS
                WHERE koders = '{kodeRs}'
                ORDER BY koderuangan DESC LIMIT 1;
            """)

            row = cursor.fetchone()

            print(row)

            if row != None:
                kodeRuangan = row[0][0:1] + str(int(row[0][1:])+1).zfill(2)
            else :
                kodeRuangan = "R01"
            
            print("koderuangan rs adalah" + kodeRuangan)

        except:
            print("exec happened on data")
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    
    context = {'kodeRuangan' : kodeRuangan}

    return JsonResponse(json.dumps(context), safe=False)

def createBed(request):
    
    if request.session.get("peran") != "admin satgas":
        return redirect("pengguna:index")
    
    if request.method == "POST":
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    INSERT INTO BED_RS (koderuangan, koders, kodebed) VALUES
                    ('{request.POST['koderuangan']}', '{request.POST['koderumahsakit']}', '{request.POST['kodebed']}')
                """)

                return redirect("roomnbed:readBed")

            except:
                messages.add_message(request, messages.WARNING, "Terdapat error")
                traceback.print_exc()  


    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT DISTINCT kode_faskes FROM RUMAH_SAKIT
            """)

            result = cursor.fetchall()

        except:
            messages.add_message(request, messages.WARNING, "Terdapat error")
            traceback.print_exc()
    
    return render(request, "roomnbed/createBed.html", {'context':result})

def kodeRuanganBed(request):
    kodeRs = request.GET['q']
    kodeRuangan = ""

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT DISTINCT koderuangan FROM RUANGAN_RS
                WHERE koders = '{kodeRs}'
            """)

            row = cursor.fetchall()

            print(row)
            
            #print("koderuangan rs adalah" + row)

        except:
            print("exec happened on data")
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    
    context = {'kodeRuangan' : row}

    return JsonResponse(json.dumps(context), safe=False)

def kodeBed(request):
    kodeRs = request.GET['rs']
    kodeRuangan = request.GET['ruangan']
    kodeBed = ''

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT DISTINCT kodebed FROM BED_RS
                WHERE koders = '{kodeRs}' and koderuangan = '{kodeRuangan}'
                ORDER BY kodebed DESC LIMIT 1;
            """)

            row = cursor.fetchone()

            print(row)

            if row != None:
                kodeBed = row[0][0:1] + str(int(row[0][1:])+1).zfill(2)
            else :
                kodeBed = "B01"
            
            print("kodebed ruangan adalah" + kodeRuangan)

        except:
            print("exec happened on data")
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()

    context = {'kodeBed' : kodeBed}

    return JsonResponse(json.dumps(context), safe=False)

def readRuangan(request):
    rows = []
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT koders, koderuangan, tipe, jmlbed, harga FROM RUANGAN_RS
            """)

            rows = cursor.fetchall()

            print(rows)
        except:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    
    return render(request, "roomnbed/readRuangan.html", {'context':rows})

def readBed(request):
    result = []
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT koderuangan, koders, kodebed FROM BED_RS
            """)
            
            rowResult = cursor.fetchall()

            cursor.execute(f"""
                SELECT koderuangan, koders, kodebed FROM RESERVASI_RS
                WHERE tglkeluar > now()::date
            """)

            undeletable = []
            deleteResult = cursor.fetchall()
            for row in deleteResult:
                undeletable.append(row)

            for row in rowResult:
                result.append([
                    row[0],
                    row[1],
                    row[2],
                    True if row not in undeletable else False
                ])

        except:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    
    return render(request, "roomnbed/readBed.html", {'context' : result})

def updateRuangan(request, param1, param2):

    if request.method == 'POST':
        with connection.cursor() as cursor:
            try:
                cursor.execute(f"""
                    UPDATE RUANGAN_RS
                    SET tipe = '{request.POST['tipe']}',
                    harga = '{request.POST['harga']}'
                    WHERE (koders, koderuangan) = ('{param1}', '{param2}')
                """)

                return redirect("roomnbed:readRuangan")
            except:
                messages.add_message(request, messages.WARNING, "Terdapat internal error")
                traceback.print_exc()

    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                SELECT koders, koderuangan, tipe, harga FROM RUANGAN_RS
                WHERE (koders, koderuangan) = ('{param1}', '{param2}')
            """)

            row = cursor.fetchall()[0]
            print(row)
        except:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    
    return render(request, "roomnbed/updateRuangan.html", {'row' : row})

def deleteBed(request, param1, param2, param3):
    with connection.cursor() as cursor:
        try:
            cursor.execute(f"""
                DELETE FROM BED_RS
                WHERE (koderuangan, koders, kodebed) = ('{param1}', '{param2}', '{param3}')
            """)
            return redirect("roomnbed:readBed")
        except:
            messages.add_message(request, messages.WARNING, "Terdapat internal error")
            traceback.print_exc()
    